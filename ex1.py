# Сергеев Артем Константинович
# Вариант 4
# Декоратор умножающий на 2 результат выполнения функции
def multiply(_sum):
    def output_func(*args):
        return _sum(*args) * 2

    return output_func


@multiply
def sum(a, b):
    return a + b


for i in range(4):
    print("значение i - " + str(i) + " | рез-т вычислений - " + str((i + 1 + i * 4) * 2))
    print("рез-т выполненяи функции с декоратором " + str(sum(i + 1, i * 4)) + "\n")
