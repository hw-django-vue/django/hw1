# Сергеев Артем Константинович
# Вариант 4
# Определите класс A, включающий:
# строку документирования класса '''Класс A''';
# метод set_a() для установки значения атрибута a;
# метод get_a() для получения значения этого атрибута.
#
# Определите класс B, включающий:
# строку документирования класса '''Класс B''';
# конструктор, инициализирующий атрибут данных b создаваемых экземпляров;
# метод get_b() для получения значения этого атрибута.
#
# Определите класс C, наследующий классы A (задача №2) и B (задача №3) и включающий:
# строку документирования класса '''Класс C = A + B''';
# конструктор, инициализирующий дополнительно атрибуты данных a и c создаваемых экземпляров;
# собственные методы set_b() и set_c() для установки значений соответствующих атрибутов;
# собственный метод get_c() для получения значения атрибута c
#
# Определите класс D, включающий:
# статический метод stat_print_dict, выводящий на экран словарь атрибутов переданного ему объекта класса;
# метод класса cls_print_dict, выводящий на экран словарь атрибутов своего класса.

class A:
    '''Класс A'''

    def set_a(self, value):
        self.a = value

    def get_a(self):
        return self.a


class B:
    '''Класс B'''

    def __init__(self, value):
        self.b = value

    def get_b(self):
        return self.b


class C(A, B):
    '''Класс C = A + B'''

    def __init__(self, a, b):
        self.set_a(a)
        super().__init__(b)

    def set_b(self, b):
        self.b = b

    def set_c(self, c):
        self.c = c

    def get_c(self):
        return self.c


class D:
    @staticmethod
    def stat_print_dict(obj):
        print(obj.__dict__)

    @classmethod
    def cls_print_dict(cls):
        print(cls.__dict__)


#Для примера
class MyClass(D):
    x = 0
    y = ''


obj = MyClass()
obj.x = 10
obj.y = 'Скрипт'
obj.z = True

D.stat_print_dict(obj)
D.cls_print_dict()





# Создание экземпляра класса A
a_instance = A()
a_instance.set_a(10)
print("Вывод: 10")
print(a_instance.get_a())  # Вывод: 10

# Создание экземпляра класса B
b_instance = B(20)

print("Вывод: 40 (унаследовано от класса B):")
print(b_instance.get_b())  # Вывод: 20

# Создание экземпляра класса C
c_instance = C(30, 40)

print("Вывод: 30 (унаследовано от класса A):")
print(c_instance.get_a())

print("Вывод: 40 (унаследовано от класса B):")
print(c_instance.get_b())

c_instance.set_c(50)

print("Вывод: 50:")
print(c_instance.get_c())
