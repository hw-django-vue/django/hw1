# Сергеев Артем Константинович
# Вариант 4
#  Напишите функцию sum_range(start, end), которая суммирует все целые числа от значения «start» до величины «end» включительно.

def sum_range(start, end):
    res = 0
    for i in range(start, end + 1):
        res += i
    return res


print(sum_range(1, 10))