# Сергеев Артем Константинович
# Вариант 4
# Напишите функцию tpl_sort(), которая сортирует кортеж, состоящий из целых чисел по возрастанию
# и возвращает его. Если хотя бы один элемент не является целым числом, то функция возвращает исходный кортеж.
def tpl_sort(_tuple):
    if all(isinstance(element, int) for element in _tuple):
        print('Кортеж не содержит нецелых чисел')
        sorted_tuple = (sorted(_tuple))
        return sorted_tuple
    else:
        print('В кортеже нецелые числа')
        return _tuple


my_tuple1 = (4, 3, 5, 7, 3, 423, 432, 12, 32, 54, 54.3)

my_tuple2 = (4, 3, 5, 7, 3, 423, 432, 12, 32, 54)

print(tpl_sort(my_tuple1))
print("\n")
print(tpl_sort(my_tuple2))
